package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;

public class VersementMapper {
	private static VersementDto versementDto;

	public static VersementDto map(Versement versement) {
		versementDto = new VersementDto();
		versementDto.setNom_prenom_emetteur(versement.getNom_prenom_emetteur());
		versementDto.setNrCompteBeneficiaire(versement.getCompteBeneficiaire().getNrCompte());
		versementDto.setDate(versement.getDateExecution());
		versementDto.setMotif(versement.getMotifVersement());
		versementDto.setMontantVersement(versement.getMontantVersement());

		return versementDto;
	}
}
