package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Utilisateur;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {
	List<Utilisateur> findAll();
}
