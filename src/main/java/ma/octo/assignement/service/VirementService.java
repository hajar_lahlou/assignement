package ma.octo.assignement.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.VirementRepository;

@Service
@Transactional
public class VirementService {

	@Autowired
	private VirementRepository virementRepository;
    Logger LOGGER = LoggerFactory.getLogger(AutiService.class);


	public List<VirementDto> findAll(){
		List<Virement> all=virementRepository.findAll();

		if (CollectionUtils.isEmpty(all)) 
		{
			return null;
		} 
		else 
		{
			List<VirementDto> allDto=new ArrayList<VirementDto>();
			for(Virement v : all) 
			{
				allDto.add(VirementMapper.map(v));
			}
			return allDto;

		}
	}

	public void save(Virement virement) {
		if(virementRepository.save(virement) != null) 
		{
			LOGGER.info(">> Virement enregistré depuis " + virement.getCompteEmetteur().getNrCompte()
					+ " vers " + virement.getCompteBeneficiaire().getNrCompte() 
					+ " d'un montant de " + virement.getMontantVirement());
		}
	}




}
