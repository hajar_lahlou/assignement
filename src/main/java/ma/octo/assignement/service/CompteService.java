package ma.octo.assignement.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.repository.CompteRepository;
@Service
@Transactional
public class CompteService {

	@Autowired
	private CompteRepository compteRepository;

	public List<Compte> findAll(){
		List<Compte> all= compteRepository.findAll();
		if (CollectionUtils.isEmpty(all)) 
		{
			return null;
		}
		else 
		{
			return all;
		}
	}

	public Compte findByNrCompte(String nrCompte) throws CompteNonExistantException {
		Compte c=compteRepository.findByNrCompte(nrCompte);
		if (c== null) 
		{
			System.out.println("Compte Non existant");
			throw new CompteNonExistantException("Compte Non existant");
		}
		return c;
	}

	public void save(Compte c1) {
		compteRepository.save(c1);

	}
}
