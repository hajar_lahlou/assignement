package ma.octo.assignement.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.repository.VersementRepository;

@Service
@Transactional
public class VersementService {
	@Autowired
	private VersementRepository versementRepository;

	Logger LOGGER = LoggerFactory.getLogger(AutiService.class);

	public List<VersementDto> findAll() {

		List<Versement> all=versementRepository.findAll();
		if (CollectionUtils.isEmpty(all)) 
		{
			return null;
		} 
		else 
		{
			List<VersementDto> allDto=new ArrayList<>();
			for(Versement v : all) 
			{
				allDto.add(VersementMapper.map(v));
			}
			return allDto;
		}
	}

	public void save(Versement versement) {
		if(versementRepository.save(versement) != null) 
		{
			LOGGER.info(">> Versement enregistré depuis " + versement.getNom_prenom_emetteur() 
			+ " vers " + versement.getCompteBeneficiaire().getNrCompte() 
			+ " d'un montant de " + versement.getMontantVersement());
		}
	}

}
