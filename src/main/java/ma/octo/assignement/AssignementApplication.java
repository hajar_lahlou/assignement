package ma.octo.assignement;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.UtilisateurService;
import ma.octo.assignement.web.VersementController;
import ma.octo.assignement.web.VirementController;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssignementApplication implements CommandLineRunner {
	
	@Autowired
	private CompteService compteService;
	@Autowired
	private UtilisateurService utilisateurService;
	@Autowired
	private VersementController versementController;
	@Autowired
	private VirementController virementController;
	
	
	public static void main(String[] args) {
		SpringApplication.run(AssignementApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Male");

		utilisateurService.save(utilisateur1);


		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setUsername("user2");
		utilisateur2.setLastname("last2");
		utilisateur2.setFirstname("first2");
		utilisateur2.setGender("Female");

		utilisateurService.save(utilisateur2);

		
		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);

		compteService.save(compte1);

		
		Compte compte2 = new Compte();
		compte2.setNrCompte("010000B025001000");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(140000L));
		compte2.setUtilisateur(utilisateur2);

		compteService.save(compte2);

		
		Virement virement = new Virement();
		virement.setMontantVirement(BigDecimal.TEN);
		virement.setCompteBeneficiaire(compte2);
		virement.setCompteEmetteur(compte1);
		virement.setDateExecution(new Date());
		virement.setMotifVirement("Assignment 2021");

		VirementDto virementDto=VirementMapper.map(virement);
		virementController.createTransaction(virementDto);
		
		
		Versement versement=new Versement();
		versement.setCompteBeneficiaire(compte2);
		versement.setNom_prenom_emetteur(utilisateur1.getLastname()+" "+utilisateur1.getFirstname());
		versement.setDateExecution(new Date());
		versement.setMontantVersement(BigDecimal.valueOf(1000));
		versement.setMotifVersement("Cadeau d'anniversaire");
	
		VersementDto versementDto=VersementMapper.map(versement);
		versementController.createTransaction(versementDto);
		

	}
}
