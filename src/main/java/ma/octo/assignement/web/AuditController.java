package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.AuditVersement;
import ma.octo.assignement.repository.AuditVersementRepository;

@RestController(value = "/audits")
public class AuditController {

	@Autowired
	AuditVersementRepository av;
	@GetMapping("lister_audit")
	List<AuditVersement> listerAudit() 
	{
		if( CollectionUtils.isEmpty(av.findAll()))
			return null;
		return  av.findAll();
	}

}
