package ma.octo.assignement.web;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.AutiService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.VersementService;

@RestController(value = "/versements")

public class VersementController {

	public static final int MONTANT_MAXIMAL = 10000;
	Logger LOGGER = LoggerFactory.getLogger(VirementController.class);

	@Autowired
	private AutiService monservice;
	@Autowired
	private VersementService versementService;
	@Autowired
	private CompteService compteService;

	@GetMapping("lister_versement")
	List<VersementDto> loadAll() {
		return versementService.findAll();
	}

	@PostMapping("/executerVersements")
	@ResponseStatus(HttpStatus.CREATED)
	public void createTransaction(@RequestBody VersementDto versementDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
		Compte f12 = compteService.findByNrCompte(versementDto.getNrCompteBeneficiaire());


		if (versementDto.getMontantVersement().equals(null)) {
			System.out.println("Montant vide");
			throw new TransactionException("Montant vide");
		} else if (versementDto.getMontantVersement().intValue() == 0) {
			System.out.println("Montant vide");
			throw new TransactionException("Montant vide");
		} else if (versementDto.getMontantVersement().intValue() < 10) {
			System.out.println("Montant minimal de virement non atteint");
			throw new TransactionException("Montant minimal de virement non atteint");
		} else if (versementDto.getMontantVersement().intValue() > MONTANT_MAXIMAL) {
			System.out.println("Montant maximal de virement dépassé");
			throw new TransactionException("Montant maximal de virement dépassé");
		}

		if (versementDto.getMotif().length() < 0) {
			System.out.println("Motif vide");
			throw new TransactionException("Motif vide");
		}


		f12.setSolde(new BigDecimal(f12.getSolde().intValue() + versementDto.getMontantVersement().intValue()));
		compteService.save(f12);

		Versement versement = new Versement();
		versement.setDateExecution(versementDto.getDate());
		versement.setCompteBeneficiaire(f12);
		versement.setNom_prenom_emetteur(versementDto.getNom_prenom_emetteur());
		versement.setMontantVersement(versementDto.getMontantVersement());
		versement.setMotifVersement(versementDto.getMotif());

		versementService.save(versement);

		monservice.auditVersement("Versement depuis " + versementDto.getNom_prenom_emetteur() + " vers " + versementDto
				.getNrCompteBeneficiaire() + " d'un montant de " + versementDto.getMontantVersement()
				.toString());
	}


}
