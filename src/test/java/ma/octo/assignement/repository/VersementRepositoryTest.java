package ma.octo.assignement.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.UtilisateurService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VersementRepositoryTest {

	@Autowired
	private VersementRepository versementRepository;
	@Autowired
	CompteService compteservice; 
	@Autowired
	UtilisateurService utilisateurService;


	@Test
	public void findAll() {
		List<Versement> findAllVersement=versementRepository.findAll();		  
		assertNotNull(findAllVersement);
		assertThat(findAllVersement.size()>0);

	}

	@Test
	public void save() {
		Versement test;
		Versement versement=new Versement();
		versement.setNom_prenom_emetteur("Lahlou Hajar");
		versement.setCompteBeneficiaire(new Compte());
		versement.setMontantVersement(BigDecimal.valueOf(10000));
		versement.setDateExecution(new Date());
		versement.setMotifVersement("test save versement");

		test =versementRepository.save(versement);
		assertNotNull(test);
		assertTrue(test.getId()>0);
	}

}
