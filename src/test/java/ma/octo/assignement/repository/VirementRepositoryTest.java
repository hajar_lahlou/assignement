package ma.octo.assignement.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VirementRepositoryTest {

	@Autowired
	private VirementRepository virementRepository;


	@Test
	public void findAll() {
		List<Virement> findAllVirements=virementRepository.findAll();
		assertNotNull(findAllVirements);
		assertThat(findAllVirements.size()>0);

	}

	@Test
	public void save() {
		Virement test;
		Virement virement=new Virement();
		virement.setCompteEmetteur(new Compte());
		virement.setCompteBeneficiaire(new Compte());
		virement.setMontantVirement(BigDecimal.TEN);
		virement.setDateExecution(new Date());
		virement.setMotifVirement("test");

		test =virementRepository.save(virement);
		assertNotNull(test);
		assertTrue(test.getId()>0);
	}


}