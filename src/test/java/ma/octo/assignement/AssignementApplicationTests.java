package ma.octo.assignement;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ma.octo.assignement.web.VersementController;
import ma.octo.assignement.web.VirementController;

@SpringBootTest
class AssignementApplicationTests {

	@Autowired
	private VersementController versementController;
	@Autowired
	private VirementController virementController;
	
	
	@Test
	void contextLoads() {
		
		assertThat(versementController).isNotNull();
		assertThat(virementController).isNotNull();

	}

}
